# Hexagonal Architecture & Vertical Slice - Nodejs + Prisma ORM

## About Project

Project to manage user roles, using a hexagonal architecture structure (also known as ports and adapters).

## Getting started

Copy file environment

```bash
cp .env.example .env
```

## Docker

Create postgres container

```bash
docker compose up -d
```

## Prisma

Execute prisma migration

```bash
npx prisma migrate dev                         //Develop
npx prisma migrate dev --name create_user      //Develop and specific name migration
npx prisma migrate deploy                      //Production
```

Open Prisma Studio

```bash
npx prisma studio
```

## Start and test

Start project

```bash
yarn dev

npm run dev
```

Test with yarn

```bash
yarn test             //Run tests usecases
yarn test:watch       //Watch tests usecases

yarn test:e2e         //Run tests controllers
yarn test:e2e:watch   //Watch tests controllers

yarn test:coverage    //Watch tests usecases
yarn test:ui          //Show tests result
```

Test with npm

```bash
npm run test            //Run tests usecases
npm run test:watch      //Watch tests usecases

npm run test:e2e        //Run tests controllers
npm run test:e2e:watch  //Watch tests controllers

npm run test:coverage   //Watch tests usecases
npm run test:ui         //Show tests result
```
