import { Role } from "@/features/role/domain/models/role";

export interface GetRole {
  getAll(): Promise<Array<Role>>;

  getById(id: string): Promise<Role>;
}
