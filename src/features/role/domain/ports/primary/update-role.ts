import { Role } from "@/features/role/domain/models/role";

export interface UpdateRole {
  update(data: Role): Promise<Role>;
}
