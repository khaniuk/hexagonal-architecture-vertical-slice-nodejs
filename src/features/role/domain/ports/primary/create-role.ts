import { Role } from "@/features/role/domain/models/role";

export interface CreateRole {
  create(data: Role): Promise<Role>;
}
