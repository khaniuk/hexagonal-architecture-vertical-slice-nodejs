import { Prisma, Role } from "@prisma/client";

export interface RoleRepository {
  findAll(): Promise<Array<Role>>;

  findById(id: string): Promise<Role | null>;

  create(data: Prisma.RoleCreateInput): Promise<Role>;

  update(data: Prisma.RoleUpdateInput): Promise<Role>;

  delete(id: string): Promise<void>;
}
