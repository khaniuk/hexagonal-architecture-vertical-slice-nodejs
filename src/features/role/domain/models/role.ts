export class Role {
  constructor(
    readonly id: string | null,
    readonly description: string,
    readonly status: boolean | null
  ) {}
}
