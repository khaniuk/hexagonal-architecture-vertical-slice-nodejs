import { DeleteRole } from "@/features/role/domain/ports/primary/delete-role";
import { RoleRepository } from "@/features/role/domain/ports/secondary/role-repository";

export class DeleteRoleUseCase implements DeleteRole {
  constructor(private roleRepository: RoleRepository) {}

  async delete(id: string): Promise<void> {
    if (!id) {
      throw new Error("Role id not valid");
    }

    await this.roleRepository.delete(id);
  }
}
