import { RoleRepositoryFactory } from "@/features/role/infrastructure/repositories/role-repository-factory";

import { CreateRoleUseCase } from "./usecases/create-role-usecase";
import { DeleteRoleUseCase } from "./usecases/delete-role-usecase";
import { GetRoleUseCase } from "./usecases/get-role-usecase";
import { UpdateRoleUseCase } from "./usecases/update-role-usecase";

export class RoleFactory {
  private static roleRepository = RoleRepositoryFactory.roleRepository();

  public static getRoleUseCase() {
    const getRole = new GetRoleUseCase(this.roleRepository);

    return getRole;
  }

  public static createRoleUseCase() {
    const createRole = new CreateRoleUseCase(this.roleRepository);

    return createRole;
  }

  public static updateRoleUseCase() {
    const updateRole = new UpdateRoleUseCase(this.roleRepository);

    return updateRole;
  }

  public static deleteRoleUseCase() {
    const deleteRole = new DeleteRoleUseCase(this.roleRepository);

    return deleteRole;
  }
}
