import { PrismaRoleRepository } from "./prisma/prisma-role-repository";

export class RoleRepositoryFactory {
  public static roleRepository() {
    const roleRepository = new PrismaRoleRepository();

    return roleRepository;
  }
}
