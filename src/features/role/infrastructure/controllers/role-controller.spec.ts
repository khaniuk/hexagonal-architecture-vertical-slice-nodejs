import { app } from "@/app";
import request from "supertest";
import { afterAll, beforeAll, describe, expect, it } from "vitest";

describe("CRUD Role (e2e)", () => {
  beforeAll(async () => {
    await app.ready();
  });

  afterAll(async () => {
    await app.close();
  });

  it("should be able to crud", async () => {
    // Create role
    const responseCreate = await request(app.server).post("/roles").send({
      description: "Manager",
    });

    expect(responseCreate.statusCode).toEqual(201);

    const roleId: string = responseCreate.body.id;

    // Update role
    const responseUpdate = await request(app.server)
      .put(`/roles/${roleId}`)
      .send({
        description: "Operator",
        status: false,
      });

    expect(responseUpdate.statusCode).toEqual(200);

    // Get roles
    const responseGetAll = await request(app.server).get("/roles").send();

    expect(responseGetAll.statusCode).toEqual(200);

    // Get role by id
    const responseGetById = await request(app.server).get(`/roles/${roleId}`);

    expect(responseGetById.statusCode).toEqual(200);

    // Delete role by id
    const responseDelete = await request(app.server).delete(`/roles/${roleId}`);

    expect(responseDelete.statusCode).toEqual(401);
  });
});
