import { User } from "@/features/user/domain/models/user";

export interface CreateUser {
  create(data: User): Promise<User>;
}
