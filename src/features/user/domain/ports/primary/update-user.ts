import { User } from "@/features/user/domain/models/user";

export interface UpdateUser {
  update(data: User): Promise<User>;
}
