import { User } from "@/features/user/domain/models/user";

export interface GetUser {
  getAll(): Promise<Array<User>>;

  getById(id: string): Promise<User>;
}
