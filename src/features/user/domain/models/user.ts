export class User {
  constructor(
    readonly id: string | null,
    readonly name: string,
    readonly email: string,
    readonly role_id: string,
    readonly password: string | null,
    readonly status: boolean | null
  ) {}
}
