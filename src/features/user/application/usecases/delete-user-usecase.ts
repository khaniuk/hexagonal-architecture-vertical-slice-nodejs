import { DeleteUser } from "@/features/user/domain/ports/primary/delete-user";
import { UserRepository } from "@/features/user/domain/ports/secondary/user-repository";

export class DeleteUserUseCase implements DeleteUser {
  constructor(private userRepository: UserRepository) {}

  async delete(id: string): Promise<void> {
    if (!id) {
      throw Error("User identify not valid");
    }

    await this.userRepository.delete(id);
  }
}
