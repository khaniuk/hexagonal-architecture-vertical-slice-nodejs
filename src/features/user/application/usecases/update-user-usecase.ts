import { EmailAlreadyExistsError } from "@/features/user/application/errors/email-already-exists-error";
import { User } from "@/features/user/domain/models/user";
import { UpdateUser } from "@/features/user/domain/ports/primary/update-user";
import { UserRepository } from "@/features/user/domain/ports/secondary/user-repository";
import { hash } from "bcryptjs";

export class UpdateUserUseCase implements UpdateUser {
  constructor(private userRepository: UserRepository) {}

  async update({
    id,
    name,
    email,
    password,
    role_id,
    status,
  }: User): Promise<User> {
    if (!id) {
      throw Error("User identify not valid");
    }

    const userResult = await this.userRepository.findById(id);
    if (userResult?.email != email) {
      const userWithSameEmail = await this.userRepository.findByEmail(email);

      if (userWithSameEmail) {
        throw new EmailAlreadyExistsError();
      }
    }

    let passwordHash = "";
    if (password) {
      passwordHash = await hash(password, 6);
    }

    const user = await this.userRepository.update(id, {
      name,
      email,
      password: passwordHash,
      role: {
        connect: {
          id: role_id,
        },
      },
      status,
    });

    return {
      id: user.id,
      name: user.name,
      email: user.email,
      password: "",
      role_id: user.role_id,
      status: user.status,
    };
  }
}
