import { UserRepositoryFactory } from "@/features/user/infrastructure/repositories/user-repository-factory";
import { CreateUserUseCase } from "./usecases/create-user-usecase";
import { DeleteUserUseCase } from "./usecases/delete-user-usecase";
import { GetUserUseCase } from "./usecases/get-user-usecase";
import { UpdateUserUseCase } from "./usecases/update-user-usecase";

export class UserFactory {
  private static userRepository = UserRepositoryFactory.userRepository();

  public static getUserUseCase() {
    const getUser = new GetUserUseCase(this.userRepository);

    return getUser;
  }

  public static createUserUseCase() {
    const createUser = new CreateUserUseCase(this.userRepository);

    return createUser;
  }

  public static updateUserUseCase() {
    const updateUser = new UpdateUserUseCase(this.userRepository);

    return updateUser;
  }

  public static deleteUserUseCase() {
    const deleteUser = new DeleteUserUseCase(this.userRepository);

    return deleteUser;
  }
}
