import { PrismaUserRepository } from "./prisma/prisma-user-repository";

export class UserRepositoryFactory {
  public static userRepository() {
    const userRepository = new PrismaUserRepository();

    return userRepository;
  }
}
