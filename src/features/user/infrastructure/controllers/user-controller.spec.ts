import { app } from "@/app";
import request from "supertest";
import { afterAll, beforeAll, describe, expect, it } from "vitest";

describe("CRUD User (e2e)", () => {
  beforeAll(async () => {
    await app.ready();
  });

  afterAll(async () => {
    await app.close();
  });

  it("should be able to crud", async () => {
    // Create role
    const responseCreateRole = await request(app.server).post("/roles").send({
      description: "Manager",
    });

    expect(responseCreateRole.statusCode).toEqual(201);

    const roleId: string = responseCreateRole.body.id;

    // Create user
    const responseCreateUser = await request(app.server).post("/users").send({
      name: "Jhon",
      email: "jhon@mail.com",
      password: "123456",
      role_id: roleId,
    });

    const userId: string = responseCreateUser.body.id;

    expect(responseCreateUser.statusCode).toEqual(201);

    // Update user
    const responseUpdateUser = await request(app.server)
      .put(`/users/${userId}`)
      .send({
        name: "Jenn",
        email: "jenn@mail.com",
        password: "123456",
        role_id: roleId,
        status: false,
      });

    expect(responseUpdateUser.statusCode).toEqual(200);

    // Get users
    const responseGetUsers = await request(app.server).get("/users").send();

    expect(responseGetUsers.statusCode).toEqual(200);

    // Get user by id
    const responseGetUserById = await request(app.server).get(`/users/${userId}`);

    expect(responseGetUserById.statusCode).toEqual(200);

    // Delete user
    const responseDeleteUser = await request(app.server).delete(
      `/users/${userId}`
    );

    expect(responseDeleteUser.statusCode).toEqual(401);

    // Delete role
    const responseDeleteRole = await request(app.server).delete(
      `/roles/${roleId}`
    );

    expect(responseDeleteRole.statusCode).toEqual(401);
  });
});
