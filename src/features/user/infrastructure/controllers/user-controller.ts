import { FastifyReply, FastifyRequest } from "fastify";
import { z } from "zod";

import { EmailAlreadyExistsError } from "@/features/user/application/errors/email-already-exists-error";
import { UserFactory } from "@/features/user/application/user-factory";
import { User } from "@/features/user/domain/models/user";

export async function getUsers(request: FastifyRequest, reply: FastifyReply) {
  let users: Array<User>;

  try {
    const getUserUseCase = UserFactory.getUserUseCase();
    users = await getUserUseCase.getAll();
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(200).send(users);
}

export async function getUserById(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const userParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = userParameterSchema.parse(request.params);

  let user: User;

  try {
    const getUserUseCase = UserFactory.getUserUseCase();
    user = await getUserUseCase.getById(id);
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(200).send(user);
}

export async function createUser(request: FastifyRequest, reply: FastifyReply) {
  const createUserBodySchema = z.object({
    name: z.string(),
    email: z.string().email(),
    password: z.string().min(6),
    role_id: z.string(),
  });

  const { name, email, password, role_id } = createUserBodySchema.parse(
    request.body
  );

  let user: User;

  try {
    const createUserUseCase = UserFactory.createUserUseCase();
    user = await createUserUseCase.create({
      id: "",
      name,
      email,
      password,
      role_id,
      status: true,
    });
  } catch (err) {
    if (err instanceof EmailAlreadyExistsError) {
      return reply.status(409).send({ message: err.message });
    }

    throw err;
  }

  return reply.status(201).send(user);
}

export async function updateUser(request: FastifyRequest, reply: FastifyReply) {
  const userParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = userParameterSchema.parse(request.params);

  const updateUserBodySchema = z.object({
    name: z.string(),
    email: z.string().email(),
    password: z.string().nullable(),
    role_id: z.string(),
    status: z.boolean(),
  });

  const { name, email, password, role_id, status } = updateUserBodySchema.parse(
    request.body
  );

  try {
    const updateUserUseCase = UserFactory.updateUserUseCase();
    await updateUserUseCase.update({
      id,
      name,
      email,
      password,
      role_id,
      status,
    });
  } catch (err) {
    if (err instanceof EmailAlreadyExistsError) {
      return reply.status(409).send({ message: err.message });
    }

    throw err;
  }

  return reply.status(200).send();
}

export async function deleteUser(request: FastifyRequest, reply: FastifyReply) {
  const userParameterSchema = z.object({
    id: z.string(),
  });

  const { id } = userParameterSchema.parse(request.params);

  try {
    const deleteUserUseCase = UserFactory.deleteUserUseCase();
    await deleteUserUseCase.delete(id);
  } catch (error) {
    if (error instanceof Error) {
      return reply.status(409).send({ message: error.message });
    }

    throw error;
  }

  return reply.status(401).send();
}
